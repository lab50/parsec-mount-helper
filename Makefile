default:

install:
	install -d $(DESTDIR)/lib/systemd/system/
	install -m 0644 parsec-mount-helper.service $(DESTDIR)/lib/systemd/system/
	install -d $(DESTDIR)/usr/sbin
	install -m 0755 sbin/parsec-mount-helper $(DESTDIR)/usr/sbin
